package org.blackcampus.core.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ezalor on 21.10.2014.
 */
public class Course {
    private List<PhotoMaterial> photoMaterials;
    private List<BookMaterial> bookMaterials;
    private final UUID groupUUID;
    private final String name;

    public Course(String name, Group group) {
        this.groupUUID = group.getUUID();
        this.name = name;
        photoMaterials = new ArrayList<PhotoMaterial>();
        bookMaterials = new ArrayList<BookMaterial>();
    }


    public String getName() {
        return name;
    }

    public UUID getGroupUUID() {
        return groupUUID;
    }
    
    public void addMaterial(PhotoMaterial material){
        photoMaterials.add(material);
    }

    public void addMaterial(BookMaterial material){
       bookMaterials.add(material);
    }

}
