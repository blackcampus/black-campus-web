package org.blackcampus.core.domain;


import java.util.UUID;

/**
 * Created by ezalor on 21.10.2014.
 */
public abstract class Material {

    protected UUID uuid;
    protected String name;
    protected String description;

    protected Course course;
    protected Group group;

    public Material(){
        this.uuid = UUID.randomUUID();
    }


    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Course getCourse() {
        return course;
    }

    public Group getGroup() {
        return group;
    }
}
