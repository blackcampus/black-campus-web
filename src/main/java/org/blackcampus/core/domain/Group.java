package org.blackcampus.core.domain;

import java.util.List;
import java.util.UUID;

/**
 * Created by ezalor on 21.10.2014.
 */
public class Group {
    private final UUID uuid;
    private final String name;
    private List<Student> students;

    public Group(String name) {
        uuid = UUID.randomUUID();
        this.name = name;
    }


    public UUID getUUID() {
       return uuid;
    }
}
