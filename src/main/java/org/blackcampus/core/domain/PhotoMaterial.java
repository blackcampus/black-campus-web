package org.blackcampus.core.domain;

/**
 * Created by ezalor on 21.10.2014.
 */
public class PhotoMaterial extends Material {
    private String URL;


    public PhotoMaterial(String url) {
        super();
        this.URL = url;
    }

    public PhotoMaterial(String url, String name, String description, Course course, Group group){
        super();
        this.name = name;
        this.description = description;
        this.course = course;
        this.group = group;
    }

    public String getURL() {
        return URL;
    }
}
