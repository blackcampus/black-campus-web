package org.blackcampus.core.domain;

/**
 * Created by ezalor on 21.10.2014.
 */
public class BookMaterial extends Material {
    private final String URL;
    private final String host;
    private final String filetype;


    public BookMaterial(String name, String description, Course course, Group group, String url, String host, String filetype) {
        super();
        this.name = name;
        this.description = description;
        this.group = group;
        this.course = course;
        this.URL = url;
        this.host = host;
        this.filetype = filetype;
    }

    public String getURL() {
        return URL;
    }

    public String getHost() {
        return host;
    }

    public String getFiletype() {
        return filetype;
    }
}
